export class Venta{

    public nombre:string;
    public producto:string;
    public cantidad:number;
    public precio:number;
    public total:number;

    constructor() {
        this.iniSale();
    }

    iniSale() {
        this.nombre = null;
        this.producto = null;
        this.cantidad = null;
        this.precio = null;
        this.total = null;
  
    }

    jsonSale(){
        return{
            nombre:this.nombre,
            producto:this.producto,
            cantidad:this.cantidad,
            precio:this.precio,
            total:this.total,
          
        }
       
    }


}