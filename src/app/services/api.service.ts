import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ApiService {
  public url = "https://pruebaapi1234.azurewebsites.net/api/HttpTrigger1?code=2VIMaFxshtJd/TZ1friwut3p5uZttOwA3EIuXsvmHaHzJ2HPal5jhQ=="
  constructor(private http: HttpClient) { }

  getData(){
    return this.http.get(this.url);
  }

}
