import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class VentaService {

  public arrayDataVenta = [];

  constructor() { }

  saveSale(venta){

    if (localStorage.getItem('registroVenta') != null) {

      var registered =  JSON.parse(localStorage.getItem('registroVenta'));
      registered.push(venta);
      localStorage.setItem('registroVenta' , JSON.stringify(registered));
      return  JSON.parse(localStorage.getItem('registroVenta'));  
   

    } else {

      this.arrayDataVenta.push(venta)
      localStorage.setItem('registroVenta', JSON.stringify(this.arrayDataVenta));
      return  JSON.parse(localStorage.getItem('registroVenta'));


    }
  }

  getSales():[] {
    return  JSON.parse(localStorage.getItem('registroVenta'));
  }

}
