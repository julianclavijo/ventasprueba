import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'moneda'
})
export class MonedaPipe implements PipeTransform {

  transform(value:any): unknown {
    var valToArray = Array.from(String(value));

    var cont = 0;
    valToArray.reverse().forEach((element,key) => {
      cont += 1;
       if (cont % 3 == 0) { 
        valToArray.splice(cont , 0 , '.')
      }
    });
    var valueCompleted = '$ '+valToArray.reverse().join('');
    console.log(valueCompleted);
    

    return valueCompleted;
  }

}
