import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from "@angular/forms/";
import { HttpClientModule } from "@angular/common/http";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { TableComponent } from './components/table/table.component';
import { RegisterSalesComponent } from './components/register-sales/register-sales.component';
import { HomeComponent } from './components/home/home.component';
import { SalesTableComponent } from './components/sales-table/sales-table.component';
import { RouterModule, ROUTES } from '@angular/router';
import { MonedaPipe } from './pipes/moneda.pipe';




@NgModule({
  declarations: [
    AppComponent,

    TableComponent,
    RegisterSalesComponent,
    HomeComponent,
    SalesTableComponent,
    MonedaPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
