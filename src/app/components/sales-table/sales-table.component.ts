import { Component, Input, OnInit } from '@angular/core';


@Component({
  selector: 'app-sales-table',
  templateUrl: './sales-table.component.html',
  styleUrls: ['./sales-table.component.scss']
})
export class SalesTableComponent implements OnInit {
  @Input() salesTable;
  constructor() { }

  ngOnInit(): void {
    console.log(this.salesTable);
  }

}
