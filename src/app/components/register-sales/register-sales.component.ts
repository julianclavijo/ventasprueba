import { Component, Input, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';

import { Venta } from "../../model/venta";
import { VentaService } from "../../services/venta.service";

@Component({
  selector: 'app-register-sales',
  templateUrl: './register-sales.component.html',
  styleUrls: ['./register-sales.component.scss']
})
export class RegisterSalesComponent implements OnInit {
  public salesModel:Venta;
  @Input() sales;

  constructor(public ventaService:VentaService) { 
    this.salesModel = new Venta();
  
  }

  ngOnInit(): void {
   this.sales =  this.ventaService.getSales();
  }

  saveSales( myForm : NgForm){
  
    this.salesModel.total = myForm.form.value.precio * myForm.form.value.cantidad;
    this.ventaService.saveSale(this.salesModel.jsonSale());
    this.ngOnInit();
    myForm.resetForm();

  }

}
