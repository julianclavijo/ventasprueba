import { Component, OnInit } from '@angular/core';
import { ApiService } from "../../services/api.service";
import { User } from "../../model/user";
@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  public dataUser:Array <{User:User}>;
  constructor(private apiService:ApiService) { }

  ngOnInit(): void {
    this.apiService.getData().subscribe((data:any)=>{
    this.dataUser = data;

    })
    


  }

}
