import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from "./components/home/home.component";
import { RegisterSalesComponent } from "./components/register-sales/register-sales.component";
import { TableComponent } from "./components/table/table.component";
const routes: Routes = [
  {path:'', component:HomeComponent},
  {path:'ventas', component:RegisterSalesComponent},
  {path:'table', component:TableComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
